import re

class StringSpliter:

    def __init__(self):
        self.text = ""

    def parse_string(self, s, regex=""):
        result = []

        if len(s) < 1:
            return result
        
        tempResult = re.split(regex, s)
        # for i in tempResult:
        #     result.append(i.strip())
        if len(tempResult[0]) > 1:
            result = tempResult
        return result

   