from string_spliter import StringSpliter

def test_split_empty_string_result_empty_array():
    # arrange
    stringToSplit = ""
    expResult = []
    result = None
    cut = StringSpliter()
                    
    # act
    result = cut.parse_string(stringToSplit)
        
    # assert
    assert result == expResult

def test_split_comma_empty_string_result_empty_array():
    # arrange
    stringToSplit = ","
    expResult = []
    result = None    
    cut = StringSpliter()

    # act
    result = cut.parse_string(stringToSplit)
    # assert
    assert result == expResult


def test_split_one_string_result_array_of_one():
    # arrange
    stringToSplit = "java"
    regex = ",\\ *"
    expResult = ["java"]
    result = None    
    cut = StringSpliter()

    #act        
    result = cut.parse_string(stringToSplit, regex)

    #assert
    assert result == expResult


#def test_comma_sep_input_expect_list_of_words():
