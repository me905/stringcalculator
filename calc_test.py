from string_calculator import StringCalculator
import sys

if __name__=="__main__":
    # cal = StringCalculator()
    expressions = sys.argv[1:]
    for expression in expressions:
        cal = StringCalculator()
        if len(cal.tokens) > 0:
            raise Exception('Calculator tokens should be None.') 
        print(expression, cal.parse(expression)) # tokenize is private
        # print(cal.tokens)
