from string_calculator import StringCalculator

def test_correct_params_expect_OP_NUM_NUM():

    # Arrange
    cut = StringCalculator()
    input_val = "+, 3, 4"
    expected_res = ["OPERATOR", "NUMBER", "NUMBER"]

    # Act
    result = cut.parse(input_val)

    # Assert
    assert result == expected_res

def test_no_OP_expect_INVOP_NUM_NUM():

    # Arrange
    cut = StringCalculator()
    input_val = "&, 3, 4"
    expected_res = ["INVALID_OPERATOR", "NUMBER", "NUMBER"]

    # Act
    result = cut.parse(input_val)

    # Assert
    assert result == expected_res